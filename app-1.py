import requests
import json
import time
import psycopg2
import psycopg2.pool
import psycopg2.extras

#from pgHelpers import *

from r7insight import R7InsightHandler
import logging

rapid7_key="85349a1e-fb3b-4ce3-9fcc-eebe9b6d8707"
log = logging.getLogger('r7insight')
log.setLevel(logging.INFO)
rapid7_handler = R7InsightHandler(rapid7_key, 'eu')
log.addHandler(rapid7_handler)

from rpc_actions import forgeOperation
from api_actions import runApiFunction
from gsheets_actions import runGSheetsFunction

# pgHelpers

def runQueryAndGetResults(connPool, queryText, numResults = 0):
    try:
        conn = connPool.getconn()
        cursor = conn.cursor()        
        cursor.execute(queryText)
        colnames = [desc[0] for desc in cursor.description]
        if (numResults > 0):
            return (colnames, cursor.fetchmany(numResults))
        else:
            return (colnames, cursor.fetchall())

    except (Exception, psycopg2.DatabaseError) as error:
        print ("Error while running PostgreSQL query: ", error)

    finally:        
        if(conn):
            cursor.close()
            connPool.putconn(conn)


def runGenericStatement(connPool, queryText, doCommit = True):
    try:
        conn = connPool.getconn()
        cursor = conn.cursor()        
        cursor.execute(queryText)
        if doCommit:
            conn.commit()

    except (Exception, psycopg2.DatabaseError) as error:
        print ("Error while running PostgreSQL insert: ", error)

    finally:        
        if(conn):
            cursor.close()
            connPool.putconn(conn)


# Expected query format: "INSERT INTO table1 (col1, col2, col3) VALUES %s",
def runMultipleInsert(connPool, queryText, values, doCommit = True):
    try:
        conn = connPool.getconn()
        cursor = conn.cursor()        
        psycopg2.extras.execute_values(cursor, queryText, values)
        if doCommit:
            conn.commit()

    except (Exception, psycopg2.DatabaseError) as error:
        print ("Error while running PostgreSQL insert: ", error)

    finally:        
        if(conn):
            cursor.close()
            connPool.putconn(conn)

# Blockchain IFTTT Code

def getBlockLevel():
    url = 'https://node.tezosapi.com/chains/main/blocks/head'
    result = requests.get(url)
    return result.json()["header"]["level"]

def getBlockTxCount():
    url = 'https://node.tezosapi.com/chains/main/blocks/head'
    result = requests.get(url)
    return len(result.json()['operations'][3])

def getAllTxRecords(blockNum):
    txRecords = []
    
    url = 'https://node.tezosapi.com/chains/main/blocks/'+str(blockNum)
    result = requests.get(url)
    result_json = result.json()['operations'][3]

    for tx in result_json:
        #print (tx)
        transfer = tx['contents'][0] # source, amount, destination
        if transfer["kind"] == "transaction":
            transfer['hash'] = tx['hash']
            txRecords.append(transfer)
            #print ("TRANSFER")
            #print (transfer)
    return txRecords

'''
def getFullBlockDetails(BlockNum):
    url = 'https://vethor-node.vechain.com/blocks/'+str(BlockNum)
    result = requests.get(url)
    return result.json()
'''

def processBlock(blockLevel, pool):
    print (blockLevel)
    txList = getAllTxRecords(blockLevel)
    #print (txList)
    simpleTxList = [(x['source'].lower(), x['destination'].lower(), x['amount'], x['hash'].lower()) for x in txList]
    print ("SIMPLE LIST")
    print (simpleTxList)
    runGenericStatement(pool, 'CREATE TEMPORARY TABLE tezos_block_overview (tx_from varchar, tx_to varchar, tx_amt varchar, tx_hash varchar)')
    runMultipleInsert(pool, 'INSERT INTO tezos_block_overview(tx_from, tx_to, tx_amt, tx_hash) VALUES %s', simpleTxList)
    
    # Process 'from' matches
    colnames, matches = runQueryAndGetResults(pool, '''
        SELECT tezos_block_overview.*, triggers_actions_view.* 
        FROM tezos_block_overview, triggers_actions_view 
        WHERE triggers_actions_view.trigger_action_active = TRUE AND
              tezos_block_overview.tx_from = triggers_actions_view.trigger_data->>'wallet_address'              
        '''
    )
    processMatches(colnames, matches, isFromMatches = True)

    # Process 'to' matches
    colnames, matches = runQueryAndGetResults(pool, '''
        SELECT tezos_block_overview.*, triggers_actions_view.* 
        FROM tezos_block_overview, triggers_actions_view 
        WHERE triggers_actions_view.trigger_action_active = TRUE AND
              tezos_block_overview.tx_to = triggers_actions_view.trigger_data->>'wallet_address'              
        '''
    )        
    processMatches(colnames, matches, isFromMatches = False)

    runGenericStatement(pool, "DROP TABLE tezos_block_overview")


def processMatches(colnames, matches, isFromMatches):
    print(colnames)
    print(matches)
#    [('0xea674fdde714fd979de3edf0f56aa9716b898ec8', '0xf53470595cf73ac290e203ab0ff91c791b42a694', '0xca940530191ac1835894e53795e2a89d6b97e06284dc04ab5573409766e5879c', 39, 'notification', 'send_email', {'email': 'ryan@fabrx.io'}, 47, 'wallet', 'coin_leaves', {'coin': 'eth', 'chain': 'ethereum', 'wallet_address': '0xea674fdde714fd979de3edf0f56aa9716b898ec8'}),
    try:
        index_action_type = colnames.index('action_type')
        index_action_subtype = colnames.index('action_subtype')
        index_action_data = colnames.index('action_data')
        index_trigger_data = colnames.index('trigger_data')
        index_trigger_action_id = colnames.index('trigger_action_id')
        for match in matches:
            print ('match')
            print (match)

            emailAddress = match[index_action_data]['email']
            
            if (match[index_action_type] == 'notification') and \
                (match[index_action_subtype] == 'send_email'):
                
                print(f"Sending email to: {emailAddress}")

                payload = {
                    'to_name': '',
                    'to_email': emailAddress,
                    'data': {
                        'wallet_address': match[index_trigger_data]['wallet_address'],
                        'chain': match[index_trigger_data]['chain'],
                        'coin': match[index_trigger_data]['coin'],
                        'action': 'Sent From' if isFromMatches else 'Sent To',
                        'id_trigger_action': match[index_trigger_action_id]
                    }
                }
                print(payload)
                r = requests.post(
                    'https://k0r1f1y1t9.execute-api.us-west-1.amazonaws.com/prod/action_email_template_hello', 
                     data=json.dumps(payload))
                print (r.status_code)

            elif (match[index_action_type] == 'webhook') and \
                (match[index_action_subtype] == 'json'):
                webhookURL = match[index_action_data]['webhook']
                print(f"Sending webhook to: {webhookURL}")
                coin_upper = match[index_trigger_data]['coin'].upper()
                payload = {
                    "dataJson": # 20456.pts-0.tezos-wallet-events
                        {
                          "trigger": match[colnames.index('trigger_subtype')],
                          "coin":coin_upper,
                          "chain":match[index_trigger_data]['chain'],
                          "txFrom": match[colnames.index('tx_from')],
                          "txTo": match[colnames.index('tx_to')],
                          "xtzAmount": match[colnames.index('tx_amt')],
                          "txnHash": match[colnames.index('tx_hash')]
                        }
                }
                print(payload)
                try:
                    params = {}
                    payload["dataJson"] = json.dumps(payload["dataJson"])
                    r = requests.post(url = webhookURL, params = params, data = payload)
                    print (r.json())
                    print (r.status_code)
                except Exception as e:
                    print (e)

            elif (match[index_action_type] == 'webhook') and \
                (match[index_action_subtype] == 'rpc'):
                webhookURL = match[index_action_data]['webhook']
                print(f"Sending webhook to: {webhookURL}")
                coin_upper = match[index_trigger_data]['coin'].upper()
                payload = {
                    "dataJson":
                        { "trigger_data": {
                          "trigger": match[colnames.index('trigger_subtype')],
                          "coin":coin_upper,
                          "chain":match[index_trigger_data]['chain'],
                          "txFrom": match[colnames.index('tx_from')],
                          "txTo": match[colnames.index('tx_to')],
                          "xtzAmount": match[colnames.index('tx_amt')],
                          "txnHash": match[colnames.index('tx_hash')]
                        }}
                }
                print(payload)
                rpc_response = ""

                if match[index_action_data]["function"]["name"] == 'forge_operation':
                    print ("getting rpc response")
                    rpc_response = forgeOperation(
                        match[index_action_data]["function"]["data"]["sourceAddress"],
                        match[index_action_data]["function"]["data"]["destinationAddress"],
                        match[index_action_data]["function"]["data"]["amount"]
                    )
                    payload["dataJson"]["rpc_response"] = rpc_response

                try:
                    params = {}
                    payload["dataJson"] = json.dumps(payload["dataJson"])
                    r = requests.post(url = webhookURL, params = params, data = payload)
                    print (r.json())
                    print (r.status_code)
                except Exception as e:
                    print (e)

            elif (match[index_action_type] == 'webhook') and \
                (match[index_action_subtype] == 'api_actions'):
                webhookURL = match[index_action_data]['webhook']
                print(f"Sending webhook to: {webhookURL}")
                coin_upper = match[index_trigger_data]['coin'].upper()
                payload = {
                    "dataJson":
                        { "trigger_data": {
                          "trigger": match[colnames.index('trigger_subtype')],
                          "coin":coin_upper,
                          "chain":match[index_trigger_data]['chain'],
                          "txFrom": match[colnames.index('tx_from')],
                          "txTo": match[colnames.index('tx_to')],
                          "xtzAmount": match[colnames.index('tx_amt')],
                          "txnHash": match[colnames.index('tx_hash')]
                        }}
                }
                print(payload)
                api_response = {}

                print ("getting api action response")
                try:
                    api_response = runApiFunction(
                        match[index_action_data]["api_function"]["url"],
                        match[index_action_data]["api_function"]["path"],
                        match[index_action_data]["api_function"]["method"],
                        match[index_action_data]["api_function"]["data"]
                    )
                    payload["dataJson"]["api_response"] = api_response
                except Exception as e:
                    print (e)

                try:
                    params = {}
                    payload["dataJson"] = json.dumps(payload["dataJson"])
                    r = requests.post(url = webhookURL, params = params, data = payload)
                    print (r.json())
                    print (r.status_code)
                except Exception as e:
                    print (e)

            elif (match[index_action_type] == 'webhook') and \
                (match[index_action_subtype] == 'google_sheets'):
                webhookURL = match[index_action_data]['webhook']
                print(f"Sending webhook to: {webhookURL}")
                email_to_send = ""
                if webhookURL == "":
                    email_to_send = match[index_action_data]['email']
                    print(f"Update: Sending email to: {email_to_send}")
                
                payload = {
                    "dataJson": {
                          "trigger": match[colnames.index('trigger_subtype')],
                          "coin":coin_upper,
                          "chain":match[index_trigger_data]['chain'],
                          "txFrom": match[colnames.index('tx_from')],
                          "txTo": match[colnames.index('tx_to')],
                          "xtzAmount": match[colnames.index('tx_amt')],
                          "txnHash": match[colnames.index('tx_hash')]
                        }
                }
                print(payload)
                api_response = {}

                print ("getting google sheets response")
                try:
                    api_response = runGSheetsFunction(
                        "https://sheets.googleapis.com/v4/spreadsheets/"+match[index_action_data]["sheets_data"]["spreadsheetId"]+"/values/"+match[index_action_data]["sheets_data"]["sheetName"]+"!"+match[index_action_data]["sheets_data"]["rows"]+"?key=AIzaSyDKjM9lKCZTTA66_dyTkrSaIThSSBaWu1s",
                        "",
                        "GET",
                        {}
                    )
                    payload["dataJson"]["api_response"] = api_response
                except Exception as e:
                    print (e)

                if webhookURL != "":
                    try:
                        params = {}
                        payload["dataJson"] = json.dumps(payload["dataJson"])
                        r = requests.post(url = webhookURL, params = params, data = payload)
                        print (r.json())
                        print (r.status_code)
                    except Exception as e:
                        print (e)

                elif email_to_send != "":

                    wallet_movement_str = "Sent To"

                    if match[colnames.index('trigger_subtype')] == "coin_leaves":
                        wallet_movement_str = "Sent From"

                    payload = {
                        "to_name": "",
                        "to_email": emailAddress,
                        "subject": coin_upper+" "+wallet_movement_str+" Wallet",
                        "text_line": coin_upper+" "+wallet_movement_str+" Wallet",
                        "main_title": coin_upper+" "+wallet_movement_str+" Wallet" + " Google Sheets Data: "+json.dumps(api_response),
                        "trigger_text": coin_upper+" "+wallet_movement_str+" " + match[index_trigger_data]['wallet_address'],
                        "action_text": "send email to "+emailAddress,
                        "id_trigger_action": match[index_trigger_action_id]
                    }
                    print(payload)
                    try:
                        r = requests.post(
                            'https://eqzuf5sfph.execute-api.us-west-1.amazonaws.com/default/general-email-action-test-1_hello', 
                            data=json.dumps(payload))
                        print (r.status_code)
                        print (r.text)
                    except Exception as e:
                        print (e)

    except ValueError:
        print("Cannot process matches - actionXXX columns not found")
        return
    
    for match in matches:
        pass




def main_real():    
    print ("real code")
    
    # set up connection pool
    schema = 'public'
    pg_pool = psycopg2.pool.SimpleConnectionPool(1, 20, host="fabrx-blockjoin-postgres1-do-user-6174050-0.db.ondigitalocean.com",database="blockjoin", user="doadmin", password="vw02kd5yc9onehr1", port="25060", options=f'-c search_path={schema}',)

    # Run a test query
    #print(runQueryAndGetResults(pg_pool, "SELECT * FROM triggers_actions_view"))
    #runGenericStatement(pg_pool, "CREATE TABLE mark1 (txFrom varchar, txTo varchar, txHash varchar)")

    # Loop until we get a new block, then print block details
    blockLevel = getBlockLevel()
    print (f"Current block: {blockLevel}")

    while(True):
        

        newblockLevel = getBlockLevel() #"BM3Su5hQ1aZ1RaFijMUxiNDJWjxTghKima9GCDv7yraYTVf9NgD"
        
        print (f"Looping between {blockLevel} and {newblockLevel}")
        if blockLevel != newblockLevel:
            try:          
                processBlock(newblockLevel, pg_pool)
            except Exception as err:
                print (f"Cannot process block: {blockLevel}, skipping it, because of exception", err)                


        blockLevel = newblockLevel

        time.sleep(60*1)  # Sleep for 60 seconds

    return

    txCount = getBlockTxCount()
    txCount = int(txCount, 16)
    print (f"Tx Count in block {blockLevel}: {txCount}")    

    txRecords = getAllTxRecords(blockLevel)
    print (f"Tx records: {txRecords}")


if __name__ == "__main__":
    main_real()